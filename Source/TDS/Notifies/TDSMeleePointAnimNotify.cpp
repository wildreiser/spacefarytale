// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSMeleePointAnimNotify.h"

void UTDSMeleePointAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
                                      const FAnimNotifyEventReference& EventReference)
{
	OnMeleePointNotify.Broadcast(MeshComp);
	Super::Notify(MeshComp, Animation, EventReference);
}
