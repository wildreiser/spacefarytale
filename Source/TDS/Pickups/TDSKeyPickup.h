// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickups/TDSBasePickup.h"
#include "Types.h"
#include "TDSKeyPickup.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API ATDSKeyPickup : public ATDSBasePickup
{
	GENERATED_BODY()
public:
	ATDSKeyPickup();

	UPROPERTY(BlueprintAssignable)
	FOnReceiveItem OnReceiveItem;

private:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                    int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
