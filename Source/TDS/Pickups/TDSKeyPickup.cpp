// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickups/TDSKeyPickup.h"
#include "Components/SphereComponent.h"
#include "TDSInventoryComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDSAIBaseCharacter.h"

ATDSKeyPickup::ATDSKeyPickup()
{
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ATDSKeyPickup::OnBeginOverlap);
}

void ATDSKeyPickup::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                   const FHitResult& SweepResult)
{
	const auto InventoryComponent = OtherActor->FindComponentByClass<UTDSInventoryComponent>();
	if (!InventoryComponent) return;

	InventoryComponent->bIsCanOpenDoor = true;
	OnReceiveItem.Broadcast();
	Destroy();
}
